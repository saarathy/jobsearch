package com.example.demo.response;

import org.springframework.http.HttpStatus;

public class UserResponse {

private String message;
private Object data;
private HttpStatus status;
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public Object getData() {
	return data;
}
public void setData(Object data) {
	this.data = data;
}
public HttpStatus getStatus() {
	return status;
}
public void setStatus(HttpStatus status) {
	this.status = status;
}
public UserResponse(String message, Object data, HttpStatus status) {
	super();
	this.message = message;
	this.data = data;
	this.status = status;
}

public UserResponse(String message, HttpStatus status) {
	super();
	this.message = message;
	this.status = status;
}

}
