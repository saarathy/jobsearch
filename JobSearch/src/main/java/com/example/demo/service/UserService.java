package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.entity.JobPosting;
import com.example.demo.entity.User;
import com.example.demo.repository.JobPostingRepo;
import com.example.demo.repository.UserExpectationRepo;
import com.example.demo.repository.UserExperienceRepo;
import com.example.demo.repository.UserRepo;
import com.example.demo.response.UserResponse;

@Service
@Component
public class UserService {

   @Autowired
   UserRepo repo;
   @Autowired
   UserExperienceRepo exprepo;
   @Autowired
   UserExpectationRepo expectrepo;
   @Autowired
   JobPostingRepo jobrepo;
   
   //postMapping
   public UserResponse SaveUser(User user) {
	   repo.save(user);
	   	UserResponse response  = new UserResponse("Sucessfully Created User",user,HttpStatus.OK);
	   	return response;
   }
   
   //postmappingforjob
   public UserResponse CreateJob(JobPosting job) {
	   UserResponse response = new UserResponse("Job Successfuly Created",jobrepo.save(job),HttpStatus.OK);
	   return response;
   }
   
   public int getLatestUserId() {
	   String candidate_Id = repo.getLatestUserId();
	   int user_id;
	   System.out.println("candidate_Id ::" +candidate_Id);
	   if (candidate_Id == null) {
		   user_id = 1;
	   } else {
		   user_id = Integer.parseInt(candidate_Id);
	   }
	   return user_id;
   }
   
//   //postwithdto
//   public UserResponse DtoUser(UserDto userdto) {
//	   UserResponse response = new UserResponse("Dto User is Created",)
//   }
   
   //GetMapping
   public UserResponse GetUser(User user) {
	   UserResponse response = new UserResponse("Got Successfully",repo.findAll(),HttpStatus.OK);
	   return response;
   }
   
   //Getmappingforjob
   public UserResponse GetJob(JobPosting job) {
	   UserResponse response = new UserResponse("Fetch the job",jobrepo.findAll(),HttpStatus.OK);
	   return response;
   }
   
   //updatePutMapping
//   public UserResponse UpdateUser(User user,Integer UserId) {
//	  // repo.updateByUserId(UserId,user);
//	   UserResponse response = new UserResponse("Updated successfully",user,HttpStatus.OK);
//	   return response;
//   } 
   
   //Disable job 
   
   public UserResponse UpdateJobStatus(Integer Id, String Active ) {
   	   repo.updateById(Id,Active);
	   UserResponse response = new UserResponse("Job is disabled",HttpStatus.OK);
	   return response;
   }
   
   //Disable user
   public UserResponse UpdateUserStatus(Integer UserId,String Active, User user) {
	  // Optional<User> user = repo.findById(UserId);
	   repo.updateByUserId(UserId, Active);
	  // UserResponse response = new UserResponse("User Status Updated",user, HttpStatus.OK);
	   UserResponse response = new UserResponse("User Status Updated", user, HttpStatus.OK);
	   return response;
   }
   //deleteMapping
   public UserResponse DeleteUser(int userId, User user) {
	   repo.deleteById(userId);
	   UserResponse response = new UserResponse("Deleted Successfully",user, HttpStatus.OK);
	   return response;
   }
   
   //delete mapping for job
   public UserResponse DeleteJob(int id,JobPosting job) {
	   jobrepo.deleteById(id);
	   UserResponse response = new UserResponse("job Deleted Successfully",job,HttpStatus.OK);
	   return response;
   }

}
  
