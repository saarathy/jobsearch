package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.UserExperience;
@Repository
public interface UserExperienceRepo extends JpaRepository<UserExperience, Integer> {

}
