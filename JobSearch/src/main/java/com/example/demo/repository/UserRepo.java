package com.example.demo.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.User;

@Repository
public interface UserRepo extends JpaRepository<User, Integer> {


	//update query for job for setting active status 
	 @Transactional
	 @Modifying
	 @Query(value = "UPDATE jobposting SET active = :active where id = :id",nativeQuery = true)
	  void updateById(int id,String active);
	 
	 @Transactional
	 @Modifying
	 @Query(value = "UPDATE candidate SET active = :active where user_id = :userid",nativeQuery = true)
     void updateByUserId(int userid,String active);
	 
	 @Transactional
	 @Query(value = "SELECT MAX(user_id)+1 FROM candidate ORDER BY user_id DESC",nativeQuery = true)
     String getLatestUserId();

}
