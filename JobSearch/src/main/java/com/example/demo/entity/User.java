package com.example.demo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "candidate")
public class User {
 @Id
 @GeneratedValue(strategy = GenerationType.IDENTITY)
 @Column(name = "user_id")
 private int UserId;
 @Column(name = "name")
 private String Name;
 @Column(name = "email_add")
 private String EmailAdd;
 @Column(name="phno")
 private long Phno;
 @Column(name ="location")
 private String Location;
 @Column(name="current_company")
 private String CurrentCompany;
 @Column(name = "current_salary")
 private long CurrentSalary;
 @Column(name="resume")
 private String Resume;
 @Column(name = "active")
 private String Active;
 
public String getActive() {
	return Active;
}
public void setActive(String active) {
	this.Active = active;
}
public int getUserId() {
	return UserId;
}
public void setUserId(int userId) {
	this.UserId = userId;
}
public String getName() {
	return Name;
}
public void setName(String name) {
	this.Name = name;
}
public String getEmailAdd() {
	return EmailAdd;
}
public void setEmailAdd(String emailAdd) {
	this.EmailAdd = emailAdd;
}
public long getPhno() {
	return Phno;
}
public void setPhno(long phno) {
	this.Phno = phno;
}
public String getLocation() {
	return Location;
}
public void setLocation(String location) {
	this.Location = location;
}
public String getCurrentCompany() {
	return CurrentCompany;
}
public void setCurrentCompany(String currentCompany) {
	this.CurrentCompany = currentCompany;
}
public long getCurrentSalary() {
	return CurrentSalary;
}
public void setCurrentSalary(long currentSalary) {
	this.CurrentSalary = currentSalary;
}
public String getResume() {
	return Resume;
}
public void setResume(String resume) {
	this.Resume = resume;
}

//onetomanyforuserexpectation
@OneToMany(targetEntity = UserExpectation.class,cascade = CascadeType.ALL)
@JoinColumn(name = "id",referencedColumnName = "user_id")
private List<UserExpectation> userExpectation;

public List<UserExpectation> getUserExpectation() {
	return userExpectation;
}
public void setUserExpectation(List<UserExpectation> userExpectation) {
	this.userExpectation = userExpectation;
}
//@OneToOne(cascade = CascadeType.ALL)
//private UserExpectation userExpectation;

//onetomanyforuserexperience
@OneToMany(targetEntity = UserExperience.class,cascade = CascadeType.ALL)
@JoinColumn(name = "id",referencedColumnName = "user_id")
private List<UserExperience> userExperience;

public List<UserExperience> getUserExperience() {
	return userExperience;
}
public void setUserExperience(List<UserExperience> userExperience) {
	this.userExperience = userExperience;
}

//onetomanyforjobposting
//@OneToMany(targetEntity = JobPosting.class,cascade = CascadeType.ALL)
//@JoinColumn(name = "JobId",referencedColumnName = "UserId")
//private List<JobPosting> jobPosting;

//public List<JobPosting> getJobPosting() {
//	return jobPosting;
//}
//public void setJobPosting(List<JobPosting> jobPosting) {
//	this.jobPosting = jobPosting;
//}
}
