package com.example.demo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "UserExperience")
public class UserExperience {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="expe_id")
private int ExpeId;
@Column(name = "domain")
private String Domain;
@Column(name = "experience")
private String Experience;
@Column(name = "rate_Yourself")
private int RateYourself;

/*public int getExpeId() {
	return ExpeId;
}
public void setExpeId(int expeId) {
	this.ExpeId = expeId;
}*/
public String getDomain() {
	return Domain;
}
public void setDomain(String domain) {
	this.Domain = domain;
}
public String getExperience() {
	return Experience;
}
public void setExperience(String experience) {
	this.Experience = experience;
}
public int getRateYourself() {
	return RateYourself;
}
public void setRateYourself(int rateYourself) {
	this.RateYourself = rateYourself;
}
}
