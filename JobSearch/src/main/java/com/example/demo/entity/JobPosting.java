package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jobposting")
public class JobPosting {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "id")
private int Id;
@Column(name="company_name")
private String CompanyName;
@Column(name = "tentative_salary")
private int TentativeSalary;
@Column(name = "location")
private String Location;
@Column(name = "required_resource")
private String RequiredResource;
@Column(name = "required_experience")
private int RequiredExperience;
@Column(name = "skill_set")
private String SkillSet;
@Column(name="job_description")
private String JobDescription;
@Column(name="active")
private String Active;
public int getId() {
	return Id;
}
public void setId(int id) {
	Id = id;
}
public String getCompanyName() {
	return CompanyName;
}
public void setCompanyName(String companyName) {
	CompanyName = companyName;
}
public int getTentativeSalary() {
	return TentativeSalary;
}
public void setTentativeSalary(int tentativeSalary) {
	TentativeSalary = tentativeSalary;
}
public String getLocation() {
	return Location;
}
public void setLocation(String location) {
	Location = location;
}
public String getRequiredResource() {
	return RequiredResource;
}
public void setRequiredResource(String requiredResource) {
	RequiredResource = requiredResource;
}
public int getRequiredExperience() {
	return RequiredExperience;
}
public void setRequiredExperience(int requiredExperience) {
	RequiredExperience = requiredExperience;
}
public String getSkillSet() {
	return SkillSet;
}
public void setSkillSet(String skillSet) {
	SkillSet = skillSet;
}
public String getJobDescription() {
	return JobDescription;
}
public void setJobDescription(String jobDescription) {
	JobDescription = jobDescription;
}
public String getActive() {
	return Active;
}
public void setActive(String active) {
	Active = active;
}
}
