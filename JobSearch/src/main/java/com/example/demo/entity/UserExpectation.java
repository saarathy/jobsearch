package com.example.demo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "UserExpectation")
public class UserExpectation {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "exp_id")
private int ExpId;
@Column(name = "exp_salary")
private long ExpSalary;
@Column(name = "location")
private String Location;
@Column(name = "work_culture")
private String WorkCulture;

/*public int getExpId() {
	return ExpId;
}
public void setExpId(int expId) {
	this.ExpId = expId;
}*/
public long getExpSalary() {
	return ExpSalary;
}
public void setExpSalary(long expSalary) {
	this.ExpSalary = expSalary;
}
public String getLocation() {
	return Location;
}
public void setLocation(String location) {
	this.Location = location;
}
public String getWorkCulture() {
	return WorkCulture;
}
public void setWorkCulture(String workCulture) {
	this.WorkCulture = workCulture;
}
}
