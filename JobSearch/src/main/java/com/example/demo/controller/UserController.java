package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.JobPosting;
import com.example.demo.entity.User;
import com.example.demo.response.UserResponse;
import com.example.demo.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/User")
public class UserController {

	@Autowired 
	UserService Service;
	
	//Postmapping
	@PostMapping("/CreateUserDetails")
	public UserResponse saveUserDetails(@RequestBody User user) {
		user.setUserId(Service.getLatestUserId());
		return Service.SaveUser(user);
	}
	
	//postforjob
	@PostMapping("/CreateJob")
	public UserResponse createJobDetails(@RequestBody JobPosting job) {
		return Service.CreateJob(job);
	}
	
	//Getmapping
	@GetMapping("/GetUserDetails")
	public UserResponse getUserDetails(User user) {
		return Service.GetUser(user);
	}
	
	//Getforjob
	@GetMapping("/GetJob")
	public UserResponse getJobDeatils(JobPosting job) {
		return Service.GetJob(job);
	}
	//Putmappings
//	@PutMapping("/UpdateUserDetails/{UserId}")
//	public UserResponse updateUserDetails(@RequestBody User user,@PathVariable Integer UserId) {
//		return Service.UpdateUser(user,UserId);
//	}
	
	
	//Disable user by update
	@PutMapping("/UpdateJob/{Id}")
	public UserResponse updateJob(@RequestBody JobPosting job,@PathVariable Integer Id) {
		String Active = job.getActive();
		if (Active.equalsIgnoreCase("No") || Active.equalsIgnoreCase("N")) {
			Active = "N";
		}
		else {
			Active = "Y";
		}
		return Service.UpdateJobStatus(Id,Active);
	}
	
	//Disable job using update
	@PutMapping("/UpdateUser/{UserId}")
	public UserResponse updateUser(@RequestBody User user,@PathVariable Integer UserId) {
		String Active = user.getActive();
		return Service.UpdateUserStatus(UserId, Active, user);
	}
	
	//Deletemapping
	@DeleteMapping("/DeleteUserDetails")
	public UserResponse deleteUserDetails(@RequestBody User user) {
		int userId = user.getUserId();
		return Service.DeleteUser(userId, user);
	}
	
	//Deleteforjob
	@DeleteMapping("/DeleteJob")
	public UserResponse deleteJob(@RequestBody JobPosting job) {
		int id = job.getId();
		return Service.DeleteJob(id,job);
	}
}
